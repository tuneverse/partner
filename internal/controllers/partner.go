package controllers

import (
	"backend-code-base-template/internal/usecases"
	"backend-code-base-template/version"
	"net/http"

	"github.com/gin-gonic/gin"
)

type PartnerController struct {
	router   *gin.RouterGroup
	useCases usecases.PartnerUseCaseImply
}

// NewPartnerController
func NewPartnerController(router *gin.RouterGroup, partnerUseCase usecases.PartnerUseCaseImply) *PartnerController {
	return &PartnerController{
		router:   router,
		useCases: partnerUseCase,
	}
}

// InitRoutes
func (partner *PartnerController) InitRoutes() {
	// partner.router.Handle(http.MethodGet, "/health", func(ctx *gin.Context) {
	// 	version.RenderHandler(ctx, partner, "HealthHandler")
	// })

	partner.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, partner, "HealthHandler")
	})
}

// HealthHandler
func (partner *PartnerController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}
