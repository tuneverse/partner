package consts

const (
	DatabaseType     = "postgres"
	AppName          = "partner"
	AcceptedVersions = "v1.0"
)

const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
)
