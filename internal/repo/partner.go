package repo

import (
	"backend-code-base-template/internal/entities"
	"database/sql"
)

type PartnerRepo struct {
	db *sql.DB
}

type PartnerRepoImply interface {
	GetPartners() []entities.Partner
}

// NewPartnerRepo
func NewPartnerRepo(db *sql.DB) PartnerRepoImply {
	return &PartnerRepo{db: db}
}

// GetPartners
func (partner *PartnerRepo) GetPartners() []entities.Partner {
	return []entities.Partner{}
}
