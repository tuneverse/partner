package usecases

import (
	"backend-code-base-template/internal/entities"
	"backend-code-base-template/internal/repo"
)

type PartnerUseCases struct {
	repo repo.PartnerRepoImply
}

type PartnerUseCaseImply interface {
	GetPartners() []entities.Partner
}

// NewPartnerUseCases
func NewPartnerUseCases(partnerRepo repo.PartnerRepoImply) PartnerUseCaseImply {
	return &PartnerUseCases{
		repo: partnerRepo,
	}
}

// GetPartners
func (partner *PartnerUseCases) GetPartners() []entities.Partner {
	return partner.repo.GetPartners()
}
